class Hello{
    void hello(){
       System.out.println("Hello");
   }
}
class Main extends Hello{
   void hello(String name){
       System.out.println("Hello "+name);
   }
   public static void main(String[] args){
       Main obj=new Main();
       obj.hello();
       obj.hello("KARTHIK");
   }
}
