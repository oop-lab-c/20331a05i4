#include<iostream>
using namespace std;
class opover {
 public:
	int real, imag;
	opover(int r = 0, int i = 0) {
        real = r; imag = i;
        }
	opover operator + (opover const &obj) {
		opover res;
		res.real = real + obj.real;
		res.imag = imag + obj.imag;
		return res;
	}
	void print() {
         cout << real << " + i" << imag << '\n'; 
         }
};

int main()
{
	opover c1(10, 5), c2(2, 4);
	opover c3 = c1 + c2;
	c3.print();
}
