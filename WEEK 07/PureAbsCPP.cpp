#include<iostream>
using namespace std;
class parent{
    public:
    virtual void hello()=0;
};
class child:public parent{
    public:
    void hello(){
        cout<<"hello friends"<<endl;
    }
};
int main(){
    child o1;
    o1.hello();
    return 0;
}