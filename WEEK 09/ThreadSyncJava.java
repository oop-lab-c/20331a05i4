class A extends Thread{
    synchronized public void run(){
       System.out.println("Thread1 started");
       for(int i=0;i<5;i++){
           System.out.println(i);
       }
       System.out.println("Thread1 ended");
   }
}
class B extends Thread{
   synchronized public void run(){
       System.out.println("Thread2 started");
       for(int i=0;i<5;i++){
           System.out.println(i*2);
       }
       System.out.println("Thread2 ended");
   }
}
public class Main extends Thread{
   public static void main(String[] args){
       A obj1=new A();
       B obj2=new B();
       obj1.start();
       obj2.start();
    
       
   }

}