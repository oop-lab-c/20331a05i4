class AccessSpecifierDemo{
    private int priVar ;
    protected int proVar;
    int pubVar ;
    void setVar(int priVar, int proVar, int pubVar){
        this.priVar = priVar;  
        this.proVar = proVar;
        this.pubVar = pubVar;
    }
    int getVar(){  
        return priVar;
    }
    int getVars(){
        return proVar;
    }
}
public class Main {
    public static void main(String[] args) {
        AccessSpecifierDemo a = new  AccessSpecifierDemo();
        a.setVar(108,206,3087);
        System.out.println("Private var = "+a.getVar());
        System.out.println("Protected var = "+a.getVars());
        System.out.println("Public var = "+a.pubVar);
    }
}
